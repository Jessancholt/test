﻿using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using TestDB.Repositories.Interfaces;

namespace Services
{
    public class Service<T> : IService<T>
        where T : class
    {
        private readonly IUnitOfWork _uow;

        public Service(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public List<T> GetAll()
        {
            return
                _uow.Repository<T>()
                .ReadAll()
                .ToList();
        }

        public List<T> GetAll(Expression<Func<T, bool>> condition)
        {
            return
                 _uow.Repository<T>()
                   .ReadAll().Where(condition).ToList();
        }

        public T GetInfo(int id)
        {
            return _uow.Repository<T>().Read(id);
        }

        public T Update(T entity)
        {
            _uow.Repository<T>().Update(entity);
            _uow.SaveChanges();
            return entity;
        }

        public void Create(T entity)
        {
            _uow.Repository<T>().Create(entity);
            _uow.SaveChanges();
        }
        public void Delete(T entity)
        {
            _uow.Repository<T>().Delete(entity);
            _uow.SaveChanges();
        }
    }
}
