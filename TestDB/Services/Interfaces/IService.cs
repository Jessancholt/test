﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Services.Interfaces
{
    public interface IService<T>
        where T : class
    {
        public List<T> GetAll();
        public List<T> GetAll(Expression<Func<T, bool>> condition);
        public T GetInfo(int id);
        public T Update(T entity);
        public void Create(T entity);
        public void Delete(T entity);
    }
}
