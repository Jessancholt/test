﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using TestDB.Models;

namespace TestDB
{
    public class TestDBContext : DbContext
    {
        public DbSet<Shop> Shops { get; set; }
        public DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var shops = new Shop[]
            {
                new Shop { Id = 1, Name = "Пятёрка", Address = "ул.Пролетарская 18а", WorkingHours = "8:00-21:00" },
                new Shop { Id = 2, Name = "ТехноЗамок", Address = "ул.Ленинская 31", WorkingHours = "10:00-19:00" },
                new Shop { Id = 3, Name = "ГомельДрев", Address = "ул.Якуба Коласа 105", WorkingHours = "09:00-18:00" }
            };
            var products = new Product[]
            {
                new Product { Id = 1, Name="Сметана", Description="Жирность 15%", ShopId = 1 },
                new Product { Id = 2, Name="Кофе", Description="Молотый", ShopId = 1 },
                new Product { Id = 3, Name="Сырок", Description="Творожный глазированный", ShopId = 1 },
                new Product { Id = 4, Name="LG G3", Description="Android, экран 5.5\" IPS (1440x2560), Qualcomm Snapdragon 801 MSM8974AC, ОЗУ 2 ГБ, флэш-память 16 ГБ, карты памяти, камера 13 Мп, аккумулятор 3000 мАч, 1 SIM", ShopId = 2 },
                new Product { Id = 5, Name="ASUS ROG Strix G G531GT", Description="15.6\" 1920 x 1080 IPS, 60 Гц, Intel Core i7 9750H 2600 МГц, 16 ГБ, SSD 512 ГБ, видеокарта NVIDIA GeForce GTX 1650 4 ГБ GDDR5", ShopId = 2 },
                new Product { Id = 6, Name="Samsung A51", Description="Android, экран 6.5\" AMOLED (1080x2400), Exynos 9611, ОЗУ 4 ГБ, флэш-память 64 ГБ, карты памяти, камера 48 Мп, аккумулятор 4000 мАч, 2 SIM", ShopId = 2 },
                new Product { Id = 7, Name="Диван", Description="П-образный диван Марсель Микровелюр Бордовый\\Бежевый", ShopId = 3 },
                new Product { Id = 8, Name="Кровать", Description="Кровать Эко 1,6 Ясень шимо", ShopId = 3},
                new Product { Id = 9, Name="Стеллаж", Description="Книжный стеллаж", ShopId = 3}
            };
            
            modelBuilder.Entity<Shop>().HasData(shops);
            modelBuilder.Entity<Product>().HasData(products);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile("appsettings.json");
            var config = builder.Build();
            string connectionString = config.GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer("Data Source=DESKTOP-TL46QKF\\JESSANCHOLTSQL;Initial Catalog=TestDB;Integrated Security=True;Connect Timeout=60;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            optionsBuilder.UseLazyLoadingProxies();
        }
    }
}
