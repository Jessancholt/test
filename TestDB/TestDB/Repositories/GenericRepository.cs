﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestDB.Repositories.Interfaces;

namespace TestDB.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T>
        where T : class
    {
        private readonly TestDBContext _context;
        public GenericRepository(TestDBContext context)
        {
            _context = context;
        }

        public T Read(int id)
        {
            return _context.Find<T>(id);
        }

        public IQueryable<T> ReadAll()
        {
            return _context.Set<T>();
        }

        public T Create(T user)
        {
            return _context.Add(user).Entity;
        }

        public T Update(T entity)
        {
            return _context.Update(entity).Entity;
        }

        public virtual void Delete(T entity)
        {
            _context.Remove(entity);
        }
    }
}
