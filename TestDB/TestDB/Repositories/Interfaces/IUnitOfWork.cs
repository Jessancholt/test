﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestDB.Repositories.Interfaces
{
    public interface IUnitOfWork
    {
        public GenericRepository<TEntity> Repository<TEntity>()
            where TEntity : class;

        public void SaveChanges();
    }
}
