﻿using System;
using System.Collections.Generic;
using System.Text;
using TestDB.Repositories.Interfaces;

namespace TestDB.Repositories
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        public readonly TestDBContext _context;

        public UnitOfWork(TestDBContext context)
        {
            _context = context;
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public GenericRepository<TEntity> Repository<TEntity>()
           where TEntity : class => new GenericRepository<TEntity>(_context);

        public void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}
