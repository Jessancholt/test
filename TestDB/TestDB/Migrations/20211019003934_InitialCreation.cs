﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TestDB.Migrations
{
    public partial class InitialCreation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Shops",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    WorkingHours = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shops", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ShopId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Products_Shops_ShopId",
                        column: x => x.ShopId,
                        principalTable: "Shops",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Shops",
                columns: new[] { "Id", "Address", "Name", "WorkingHours" },
                values: new object[] { 1, "ул.Пролетарская 18а", "Пятёрка", "8:00-21:00" });

            migrationBuilder.InsertData(
                table: "Shops",
                columns: new[] { "Id", "Address", "Name", "WorkingHours" },
                values: new object[] { 2, "ул.Ленинская 31", "ТехноЗамок", "10:00-19:00" });

            migrationBuilder.InsertData(
                table: "Shops",
                columns: new[] { "Id", "Address", "Name", "WorkingHours" },
                values: new object[] { 3, "ул.Якуба Коласа 105", "ГомельДрев", "09:00-18:00" });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Description", "Name", "ShopId" },
                values: new object[,]
                {
                    { 1, "Жирность 15%", "Сметана", 1 },
                    { 2, "Молотый", "Кофе", 1 },
                    { 3, "Творожный глазированный", "Сырок", 1 },
                    { 4, "Android, экран 5.5\" IPS (1440x2560), Qualcomm Snapdragon 801 MSM8974AC, ОЗУ 2 ГБ, флэш-память 16 ГБ, карты памяти, камера 13 Мп, аккумулятор 3000 мАч, 1 SIM", "LG G3", 2 },
                    { 5, "15.6\" 1920 x 1080 IPS, 60 Гц, Intel Core i7 9750H 2600 МГц, 16 ГБ, SSD 512 ГБ, видеокарта NVIDIA GeForce GTX 1650 4 ГБ GDDR5", "ASUS ROG Strix G G531GT", 2 },
                    { 6, "Android, экран 6.5\" AMOLED (1080x2400), Exynos 9611, ОЗУ 4 ГБ, флэш-память 64 ГБ, карты памяти, камера 48 Мп, аккумулятор 4000 мАч, 2 SIM", "Samsung A51", 2 },
                    { 7, "П-образный диван Марсель Микровелюр Бордовый\\Бежевый", "Диван", 3 },
                    { 8, "Кровать Эко 1,6 Ясень шимо", "Кровать", 3 },
                    { 9, "Книжный стеллаж", "Стеллаж", 3 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Products_ShopId",
                table: "Products",
                column: "ShopId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Shops");
        }
    }
}
