﻿using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Test.Models;
using TestDB;
using TestDB.Models;

namespace Test.Controllers
{
    public class ShopController : Controller
    {
        private readonly IService<Shop> _shopService;
        public ShopController(IService<Shop> shopService)
        {
            _shopService = shopService;
        }
        [HttpGet]
        public IActionResult GetInfo(int id)
        {
            var shop = _shopService.GetInfo(id);
            var model = new ShopInfo
            {
                Id = shop.Id,
                Name = shop.Name,
                Address = shop.Address,
                WorkingHours = shop.WorkingHours
            };

            return View(model);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var shops = _shopService.GetAll();

            var model = shops
                .Select(u => new ShopInfo
                {
                    Id = u.Id,
                    Name = u.Name,
                    Address = u.Address,
                    WorkingHours = u.WorkingHours
                });

            return View(model);
        }

        [HttpPost]
        public IActionResult ShopInfo(ShopInfo info)
        {
            var shop = _shopService.GetInfo(info.Id);
            shop.Name = info.Name;
            shop.Address = info.Address;
            shop.WorkingHours = info.WorkingHours;

            _shopService.Update(shop);
            return Json(new { success = "Успешно" });
        }

        [HttpPost]
        public IActionResult CreateShop(ShopInfo info)
        {
            var shop = new Shop()
            {
                Name = info.Name,
                Address = info.Address,
                WorkingHours = info.WorkingHours
            };

            _shopService.Create(shop);
            return Redirect(Url.Action("GetAll", "Shop"));
        }

        public IActionResult CreatePage()
        {
            return View();
        }

        public IActionResult DeleteShop(ShopInfo info)
        {
            var shop = _shopService.GetInfo(info.Id);
            _shopService.Delete(shop);
            return RedirectToAction("GetAll", "Shop");
        }
    }
}
