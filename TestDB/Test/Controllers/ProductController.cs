﻿using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Test.Models;
using TestDB;
using TestDB.Models;

namespace Test.Controllers
{
    public class ProductController : Controller
    {
        private readonly IService<Product> _productService;
        private readonly IService<Shop> _shopService;
        public ProductController(IService<Product> productService, IService<Shop> shopService)
        {
            _productService = productService;
            _shopService = shopService;
        }
        [HttpGet]
        public IActionResult GetInfo(int id)
        {
            var product = _productService.GetInfo(id);
            var model = new ProductInfo
            {
                Id = product.Id,
                Name = product.Name,
                Description = product.Description
            };

            return View(model);
        }

        [HttpGet]
        public IActionResult GetAll(int id)
        {
            var products = _shopService.GetAll().FirstOrDefault(u => u.Id == id).Products;

            var model = products
                .Select(u => new ProductInfo
                {
                    Id = u.Id,
                    Name = u.Name,
                    Description = u.Description
                });
            ProductViewModel viewModel = new ProductViewModel
            {
                Products = model,
                ShopId = id
            };

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult ProductInfo(ProductInfo info)
        {
            var product = _productService.GetInfo(info.Id);
            product.Name = info.Name;
            product.Description = info.Description;

            _productService.Update(product);
            return Json(new { success = "Успешно" });
        }

        [HttpPost]
        public IActionResult CreateProduct(ProductInfo info)
        {
            var shop = _shopService.GetAll().FirstOrDefault(u => u.Id == info.ShopId);
            var product = new Product()
            {
                Name = info.Name,
                Description = info.Description,
                ShopId = info.ShopId
            };
            shop.Products.Add(product);

            _productService.Create(product);
            return RedirectToAction("GetAll", "Product", new { Id = info.ShopId });
        }

        public IActionResult CreatePage(ProductInfo info)
        {
            return View(info);
        }

        public IActionResult DeleteProduct(ProductInfo info)
        {
            var product = _productService.GetInfo(info.Id);
            _productService.Delete(product);
            return RedirectToAction("GetAll", "Product", new { Id = info.ShopId });
        }
    }
}
