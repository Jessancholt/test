﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Models
{
    public class ProductInfo
    {
        public int Id { get; set; }
        [Required(ErrorMessage = @"Поле ""Название"" должно быть заполнено")]
        [Display(Name = "Название")]
        public string Name { get; set; }
        [Display(Name = "Описание")]
        public string Description { get; set; }
        public int ShopId { get; set; }
    }
}
