﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Models
{
    public class ShopInfo
    {
        public int Id { get; set; }
        [Required(ErrorMessage = @"Поле ""Наименование"" должно быть заполнено")]
        [Display(Name = "Наименование")]
        public string Name { get; set; }
        [Required(ErrorMessage = @"Поле ""Режим работы"" должно быть заполнено")]
        [Display(Name = "Режим работы")]
        public string WorkingHours { get; set; }
        [Required(ErrorMessage = @"Поле ""Адрес"" должно быть заполнено")]
        [Display(Name = "Адрес")]
        public string Address { get; set; }
    }
}
