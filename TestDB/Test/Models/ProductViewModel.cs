﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Models
{
    public class ProductViewModel
    {        
        public IEnumerable<ProductInfo> Products { get; set; }
        public int ShopId { get; set; }
    }
}
